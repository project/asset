<?php

/**
 * @file
 * Hooks provided by the Asset module.
 */

/**
 * Alter the list of asset supported field types to be overridden.
 *
 * @param array $types
 *   Types is an array with field type as a key and list of columns to be handled as a value.
 */
function hook_asset_field_types_alter(&$types) {
  // Adding entityreference to supported field types.
  $types['entityreference'] = array('target_id');
}
